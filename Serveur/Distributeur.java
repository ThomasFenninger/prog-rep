import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class Distributeur implements ServiceDistributeur{

  private ArrayList<ServiceGriseur> liste ;

  public Distributeur(){
    this.liste =new ArrayList<>();
  }

  public void connecteMachine(ServiceGriseur sc)throws RemoteException{
    this.liste.add(sc);
    System.out.println("Un noeud se connecte ("+this.liste.size()+")");
  }

  @Override
  public byte[] griseImage(byte[] img)throws RemoteException {
    System.out.println("Une image a grisee a ete recu : "+img.length+" bytes");
    //diviser l'image en part égale
    final int part = 100;

    try {
      BufferedImage biOut = ImageIO.read(new ByteArrayInputStream(img));

      final int largeur = biOut.getWidth()/part;
      int indexMach =0;
      for(int i = 0; i <= part; i++){
        BufferedImage biIn;
        if(i==part){
          biIn = biOut.getSubimage(biOut.getWidth()-biOut.getWidth()%part,0,biOut.getWidth()%part,biOut.getHeight());
        }else{
          biIn = biOut.getSubimage(i*largeur,0,largeur,biOut.getHeight());
        }


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(biIn,"jpg",baos);
        byte[] output = null;

        while(output == null){
          if(indexMach > this.liste.size()){
            indexMach = 0;
          }
          try {
          ServiceGriseur sc = this.liste.get(indexMach++);

            output = sc.calculNiveau(baos.toByteArray());
          }catch (RemoteException re){
            this.liste.remove(indexMach);
          }catch (java.lang.IndexOutOfBoundsException ioobe){
            indexMach =0;
          }
        }
        BufferedImage tmp = ImageIO.read(new ByteArrayInputStream(output));
        /*
        JFrame jf = new JFrame("Griseur");
        jf.add(new JLabel(new ImageIcon(tmp)));
        jf.pack();
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setVisible(true);
        */
        System.out.println("Partie d'image recu : "+output.length+" bytes");
        if(i==part){
          biOut.createGraphics().drawImage(tmp,biOut.getWidth()-biOut.getWidth()%part,0,biOut.getWidth()%part,biOut.getHeight(),Color.WHITE,null);
        }else{
          biOut.createGraphics().drawImage(tmp,i*largeur,0,largeur,biOut.getHeight(),Color.WHITE,null);
        }


      }
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ImageIO.write(biOut,"jpg",baos);
      System.out.println("L'image grisee est renvoyee au client");
      return baos.toByteArray();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }


}
