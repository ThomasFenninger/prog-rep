import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServiceDistributeur extends Remote{
    public void connecteMachine(ServiceGriseur sc) throws RemoteException;
    public byte[] griseImage(byte[] i)throws RemoteException;
}
