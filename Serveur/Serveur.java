import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

public class Serveur{
  public static void main ( String args[] ) throws RemoteException{

    int port=1099;
    if(args.length >= 1){
      port = Integer.parseInt(args[0]);
    }
    Distributeur d = new Distributeur();
    ServiceDistributeur sd = (ServiceDistributeur) UnicastRemoteObject.exportObject(d,port);
    Registry reg = LocateRegistry.createRegistry(port);
    reg.rebind("distributeur",sd);

  }
}
