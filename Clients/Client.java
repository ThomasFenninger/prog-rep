import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client{
  public static void main(String args[]) throws Exception {
    String ip ="";
    int port=1099;
    if(args.length < 1) {
      System.out.println("Il faut préciser le nom de fichier");
    throw new Exception();
    }else if(args.length < 2) {
      System.out.println("Il faut preciser l'addresse IP");
      ip = args[1];
      throw new Exception();
    }else if(args.length == 3) {
      port = Integer.parseInt(args[2]);
    }

    //System.out.println(port);
    Registry reg = LocateRegistry.getRegistry(ip,port);
    ServiceDistributeur sd = (ServiceDistributeur) reg.lookup("distributeur");

    File f = new File(args[0]);
    BufferedImage bi = ImageIO.read(f);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ImageIO.write(bi,"jpg",baos);

    byte[] output = sd.griseImage(baos.toByteArray());

    BufferedImage biOut = ImageIO.read(new ByteArrayInputStream(output));

    JFrame jf = new JFrame("Griseur");
    jf.add(new JLabel(new ImageIcon(biOut)));
    jf.pack();
    jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jf.setVisible(true);
  }
}
