import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Noeud {
    public static void main(String args[])throws Exception{
        // on cherche le registry du serveur
        // on se connecte sur le distributeur avec notre classe de calcul
        //fini
        String ip = "";
        int port = 1099;

        if(args.length >= 1){
            ip = args[0];
            //System.out.println(ip);
        }else{
            throw new Exception("Il faut mettre une addresse ip");
        }

        if(args.length == 2){
            port = Integer.parseInt(args[1]);
        }

        Registry reg = LocateRegistry.getRegistry(ip,port);
        ServiceDistributeur sd = (ServiceDistributeur)reg.lookup("distributeur");

        Griseur c = new Griseur(); /* Créer une instance de A */
        ServiceGriseur sc = (ServiceGriseur) UnicastRemoteObject.exportObject(c, 0); /* Un_port = un entier particulier ou 0 pour auto-assigné */
        sd.connecteMachine(sc);
    }
}
