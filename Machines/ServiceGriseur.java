import java.awt.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface ServiceGriseur extends Remote{
  public byte[] calculNiveau(byte[] input)throws RemoteException;
}
