import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;

public class Griseur implements ServiceGriseur {
    public byte[] calculNiveau(byte[] input) throws RemoteException{
      if(input == null){
        return null;
      }
      System.out.println("Une partie d'image a ete recu :" + input.length + " bytes");

      try {
        BufferedImage bi = ImageIO.read(new ByteArrayInputStream(input));
        for(int x = 0 ; x < bi.getWidth() ; x++){
          for(int y = 0 ; y < bi.getHeight() ; y++){
            int rgb = bi.getRGB(x,y);
            int r = (rgb >> 16) & 0xFF;
            int g = (rgb >> 8) & 0xFF;
            int b = (rgb & 0xFF);
            int moy = (r+g+b)/3;
            Color gray = new Color(moy,moy,moy);
            bi.setRGB(x,y,gray.getRGB());
          }
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi,"jpg",baos);
        return baos.toByteArray();
      } catch (IOException e) {
        e.printStackTrace();
      }

      return null;
    }
}
